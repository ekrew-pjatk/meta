# Meta

meta projekt zawiera ogólny [issue board](https://gitlab.com/ekrew-pjatk/meta/-/boards) 
board przeznaczony dla tasków związanych z dokumentami etc  
repo służy także do postawienia wszystkich komponentów systemu w dockerach

## repozytoria

aby pobrać wymagane repozytoria można użyć skryptu lub wykonać  

```
git clone git@gitlab.com:ekrew-pjatk/ekrewniaczek-app.git
git clone git@gitlab.com:ekrew-pjatk/eKrewni-rckik.git
```

## api host

w pliku _.env_ w ekrewniaczek-app/  
ustawić `VUE_APP_BASE_URL=<API_HOST>/ekrewni`

## docker setup

większość jest automatycznie zapewnione przez docker-compose  

## logowanie się na użytkownika centrum

w eKrewni-rckik/eKrewni/_centerusers.txt_  
są wygenerowane przykładowe dane do logowania się
