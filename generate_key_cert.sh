#!/bin/sh

for service in api ekrewniaczek ekrewniak; do
  openssl req -x509 -newkey rsa:4096 \
    -keyout ssl/ekrewni.${service}.key \
    -out ssl/ekrewni.${service}.pem -days 365 -nodes \
    <<< $'PL\nPomeranian Voivodeship\nGdańsk\nek\nek\nek\nekrew@ekrwinki.com\n'
done
