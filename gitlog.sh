#!/bin/sh

clean_up() {
	rm gitlog.*txt
}
trap clean_up EXIT


git log --date=iso --pretty=format:"%h%x09%an%x09%ad%x09%s" | gawk '{gsub(/\+[0-9]{4}/, "#"); print}' > gitlog.txt

gawk 'BEGIN {FS="#"} {print $1}' gitlog.txt \
	| sed -e 's/ Śledziona//' -e 's/sdnkr/Karol/' \
        -e 's/Krzysztof/Krzysiek/' -e 's/Krzysiek/Krzyś/' \
        -e 's/ Hawliczek//' -e 's/Ania/AniaP/' \
	| sed -e 's/\t/ /' > gitlog.hash.time.txt

gawk 'BEGIN {FS="#"} {split($1, c, " "); print c[1], $2}' gitlog.txt \
	| sed 's/\t//' \
	| gawk '{h=$1; $1=""; print h, "\"" substr($0,2) "\""}' > gitlog.hash.message.txt

join gitlog.hash.time.txt gitlog.hash.message.txt
