DO $$
BEGIN
    FOR _ IN 0..40 LOOP
        INSERT INTO alerts (
           id, alert_id,
           date,
           message,
           bloodgroups_id, centerusers_id
        )
        VALUES (
           nextval('hibernate_sequence'),
           (SELECT uuid_generate_v4()::text),
           now(),
           (SELECT string_agg(t, ' ') AS sentence
            FROM ( SELECT t FROM ipsum AS words
                   ORDER BY random() limit 5
                 ) AS loremipsum),
           (SELECT id AS bid FROM bloodgroups
            ORDER BY random() LIMIT 1),
           (SELECT id AS cuid FROM centerusers
            WHERE centers_id IS NOT NULL
            ORDER BY random() LIMIT 1)
        );
    END LOOP;
END
$$ LANGUAGE plpgsql;
