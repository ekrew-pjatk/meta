DO $$
BEGIN
    FOR _ IN 0..20 LOOP
        INSERT INTO centermessages (
            id, center_message_id,
            date_time,
            text,
            centerusers_id
        ) VALUES (
            nextval('hibernate_sequence'),
            (SELECT uuid_generate_v4()::text),
            now(),
            (SELECT string_agg(t, ' ') AS sentence
             FROM ( SELECT t FROM ipsum AS words
                    ORDER BY random() limit 5
                  ) AS loremipsum),
            (SELECT id AS cuid FROM centerusers
             WHERE centers_id IS NOT NULL
             ORDER BY random() LIMIT 1)
        );
    END LOOP;
END
$$ LANGUAGE plpgsql;
